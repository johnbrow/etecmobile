import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.page.html',
  styleUrls: ['./camera.page.scss'],
})
export class CameraPage implements OnInit {
  foto : any; 
  constructor(private camera : Camera) { }
  
  ngOnInit() {
  }
  
  obterFoto () {
    const options: CameraOptions = {
      quality: 50, //0-100
      destinationType: this.camera.DestinationType.DATA_URL, //Base64
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    
    this.camera.getPicture(options).then((imageData) =>{
      // imageData e uma string codificada em base64 ou em um URI
      // ifit base64(DATA_URL):
      let base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.foto = base64Image; //recebe a foto em base64
    }, (err) => {
      //HANDLE ERROR
      console.log("Não foi possivel carregar a foto");
    });
    
  }
  
}
