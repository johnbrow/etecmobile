import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: 'menu',
    pathMatch: 'full'
  },
  {
    path: 'botao',
    loadChildren: () => import('./botao/botao.module').then( h => h.botaoPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'selects',
    loadChildren: () => import('./selects/selects.module').then( m => m.SelectsPageModule)
  },
  {
    path: 'camera',
    loadChildren: () => import('./camera/camera.module').then( m => m.CameraPageModule)
  },
  {
    path: 'accelerometer',
    loadChildren: () => import('./accelerometer/accelerometer.module').then( m => m.AccelerometerPageModule)
  },
  {
    path: 'giroscopio',
    loadChildren: () => import('./giroscopio/giroscopio.module').then( m => m.GiroscopioPageModule)
  },
  {
    path: 'geolocation',
    loadChildren: () => import('./geolocation/geolocation.module').then( m => m.GeolocationPageModule)
  }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
