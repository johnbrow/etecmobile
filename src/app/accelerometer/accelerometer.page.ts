import { Component, OnInit } from '@angular/core';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion/ngx';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-accelerometer',
  templateUrl: './accelerometer.page.html',
  styleUrls: ['./accelerometer.page.scss'],
})
export class AccelerometerPage implements OnInit {

  // declarations
  eixoX: any;
  eixoY: any;
  eixoZ: any;
  subscription: any;

  constructor(private deviceMotion: DeviceMotion) { }

  ngOnInit() {
  }

  //ligar o acelerometro 
  obterAcelerometro() {
    let options = { frequency: 500 };
    var subscription = this.deviceMotion.watchAcceleration(options)
    .subscribe((acceleration: DeviceMotionAccelerationData) => {
      // capturando os valores das variaveis disponibilizadas pelo metodo watch
      this.eixoX = acceleration.x;
      this.eixoY = acceleration.y;
      this.eixoZ = acceleration.z;

    });
  }

  // desligar o acelerometro
  desligarAcelerometro() {
    this.subscription.unsubscribe();
  }



}
