import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccelerometerPageRoutingModule } from './accelerometer-routing.module';

import { AccelerometerPage } from './accelerometer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccelerometerPageRoutingModule
  ],
  declarations: [AccelerometerPage]
})
export class AccelerometerPageModule {}
