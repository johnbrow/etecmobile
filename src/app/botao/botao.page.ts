import { Component } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-botao',
  templateUrl: 'botao.page.html',
  styleUrls: ['botao.page.scss'],
})
export class BotaoPage {
  
  constructor(private alertCtrl: AlertController, private navCtrl: NavController) {}
  
  // Metodo que escreve na console
  // Recebe como perimetro uma string
  
  HelloConsole(texto : string){
    console.log(texto)
  }
  OpenPage(pagina : string){
    this.navCtrl.navigateForward(`/${pagina}`);
  }
  
  async showMessageOk(header_msg:string,sub_header: string, message: string, acao:string ){
    const alert = await this.alertCtrl.create({
      header: header_msg,
      subHeader: sub_header,
      message: message,
      buttons: [
        {
        text: 'Ok',
        role: 'ok',
        handler: ()=>{
          this.OpenPage('selects')
        }
       },
       {
        text : 'Cancelar',
        role : 'cancelar',
        handler: ()=> {
          console.log("o Botao de cancelar foi clicado")
        }
      }
      ]
  });
  
  await alert.present();
  const result = await alert.onDidDismiss();
  console.log(result);
}

alertOk(){
  this.showMessageOk("Teste do Alerta","Ok vai para select", "se cancelar olhe o console","O botao foi clikado" )
}

}
