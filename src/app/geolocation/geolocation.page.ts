import { Component, OnInit } from '@angular/core';

import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.page.html',
  styleUrls: ['./geolocation.page.scss'],
})
export class GeolocationPage implements OnInit {
  
  longitude: any;
  latitude : any;
  altitude : any;
  velocidade: any ;

  constructor(private geolocation: Geolocation) { }
  
  ngOnInit() {
    this.verLocalizacao();
  }
  
  verLocalizacao() {
    let options = { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true };
    let watch = this.geolocation.watchPosition(options);
    watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      this.latitude   =  data.coords.latitude;
      this.longitude  =  data.coords.longitude;
      this.altitude   =  data.coords.altitude;
      this.velocidade =  data.coords.speed;
    });
  }

}
