import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectsPage } from './selects.page';

describe('SelectsPage', () => {
  let component: SelectsPage;
  let fixture: ComponentFixture<SelectsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
